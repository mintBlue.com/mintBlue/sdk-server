# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [9.3.2] - 2025-02-06

- Upgraded @mintblue/sdk to version 9.3.2

## [8.1.0] - 2024-05-30

- Upgraded @mintblue/sdk to version 8.1.0
- Package can be used as ESM and CJS

## [7.0.0] - 2024-03-04

- Upgraded @mintblue/sdk to version 7.0.0

## [4.1.1] - 2023-08-16

- Fixed package-lock.json being in sync with package.json

## [4.1.0] - 2023-08-16

- Upgraded @mintblue/sdk to version 4.1.0
- Increased limit of JSON parser to 50 MB

## [4.0.1] - 2023-08-15

- Upraded @mintblue/sdk to version 4.0.1

## [3.0.3] - 2023-05-18

- Upgraded @mintblue/sdk to version 3.0.3

## [3.0.0] - 2023-05-17

- Upgraded @mintblue/sdk to version 3.0.0

## [1.3.5] - 2023-04-21

- Upgraded @mintblue/sdk to version 1.3.5

## [1.3.4] - 2023-04-21

- Upgraded @mintblue/sdk to version 1.3.4

## [1.3.3] - 2023-04-21

- Upgraded @mintblue/sdk to version 1.3.3

## [1.3.0] - 2023-04-20

- Upgraded @mintblue/sdk to version 1.3.0

## [1.2.0] - 2022-12-07

- Upgraded @mintblue/sdk to version 1.2.0

## [1.0.6] - 2022-10-12

- Upgraded @mintblue/sdk to version 1.0.6
- Upgraded dependencies

## [1.0.5] - 2022-10-10

### Fixed

- NPM package now contains the correct Git repository URL

## [1.0.4] - 2022-09-20

### Added

- Automatic docker releases via Gitlab CI

## [1.0.3] - 2022-09-19

### Added

- Docker release mintbluedevs/sdk-server

## [1.0.0] - 2022-09-14

### Added

- Run the mintBlue SDK in a standalone JSONRPC server
- Use SDK functionality over HTTP in any programming language
