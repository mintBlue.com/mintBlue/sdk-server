import type { Server } from 'http';

import { Mintblue, MintblueOptions } from '@mintblue/sdk';
import BodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import type { Application } from 'express';
import jayson from 'jayson';

const json = BodyParser.json;

function getSDKToken(req: express.Request): string {
  const header = req.headers['mintblue-sdk-token'] || null;
  return header;
}
export const reviver = (_key, value) => {
  if (typeof value === 'string' && value.startsWith('base64:')) {
    return Buffer.from(value.split('base64:')[1], 'base64');
  } else {
    return value;
  }
};

export const replacer = (_key, value) => {
  if (value && typeof value === 'object' && value.type === 'Buffer' && Array.isArray(value.data)) {
    return `base64:${Buffer.from(value.data).toString('base64')}`;
  } else {
    return value;
  }
};

export async function createContextMiddleware(server: jayson.Server, createOptions?: MintblueOptions) {
  const contextMiddleware = async (req, res, next) => {
    let mintBlueInstance: Mintblue = null;

    if (createOptions) {
      try {
        mintBlueInstance = await Mintblue.create(createOptions);
      } catch (e) {
        const message = 'Failed to instantiate mintBlue SDK in standalone mode. Check if your SDK token is valid.';
        console.log(e);
        console.log(message);
        return next(server.error(500, message, null));
      }
    }

    //  405 method not allowed if not POST
    if (!jayson.utils.isMethod(req, 'POST')) {
      return next(server.error(405, 'GET not allowed'));
    }

    // 415 unsupported media type if Content-Type is not correct
    if (!jayson.utils.isContentType(req, 'application/json')) {
      return next(server.error(415, 'Content-Type needs to be application/json'));
    }

    // body does not appear to be parsed, 500 server error
    if (!req.body || typeof req.body !== 'object') {
      return next(server.error(500, 'Request body must be parsed'));
    }

    let mintBlue: Mintblue = null;

    const handleCreateError = (e) => {
      const status = e.response ? e.response.status : 500;
      const message = e.response && e.response.data ? e.response.data.message : e.message;

      return next(server.error(status, message));
    };
    if (createOptions) {
      mintBlue = mintBlueInstance;
    } else {
      const sdkTokenFromHeader = getSDKToken(req);
      if (!sdkTokenFromHeader) {
        return next(server.error(400, 'Missing SDK token'));
      }

      mintBlue = await Mintblue.create({
        token: sdkTokenFromHeader,
        url: req.headers['mintblue-sdk-url'] || 'https://api.mintblue.com',
      }).catch(handleCreateError);
    }

    const context = { req, mintBlue };

    try {
      server.call(req.body, context, function (err, result) {
        if (err) {
          const error = err.error as jayson.JSONRPCError;
          return next(server.error(error.code, error.message, error.data));
        }

        jayson.utils.JSON.stringify(result, { replacer }, function (err, body) {
          if (err) {
            return next(server.error(500, err.message));
          }

          // empty response?
          if (body) {
            const headers = {
              'content-length': Buffer.byteLength(body, 'utf-8'),
              'content-type': 'application/json; charset=utf-8',
            };

            res.writeHead(200, headers);
            res.write(body);
          } else {
            res.writeHead(204);
          }

          res.end();
        });
      });
    } catch (e) {
      next(server.error(500, e.message));
    }
  };

  return contextMiddleware;
}

export function createMethods() {
  const propertyNames = Object.getOwnPropertyNames(Mintblue.prototype).filter((property) => property !== 'constructor');
  const methods = {};
  for (const method of propertyNames) {
    methods[method] = async (args, context, callback) => {
      try {
        const res = await context.mintBlue[method](args);
        callback(null, res);
      } catch (e) {
        if (e.response && e.response.data && e.response.data.validation) {
          callback({
            code: e.response.status,
            message: e.response.data.message || e.response.data.error,
            data: e.response.data.validation,
          });
        } else if (e.response) {
          callback({ code: e.response.status, message: e.response.data.message || e.response.data.error });
        } else {
          callback({ code: 500, message: 'Internal Server Error' });
        }
      }
    };
  }

  methods['returnBuffer'] = (_args, _context, callback) => {
    callback(null, Buffer.from('hey'));
  };

  return methods;
}

export async function createRouter(createOptions?: MintblueOptions) {
  const router = express.Router();

  const server = new jayson.Server(createMethods(), {
    useContext: true,
  });

  router.use(cors({ methods: ['POST'] }));
  router.use(json({ reviver, limit: '50mb' }));
  router.use(await createContextMiddleware(server, createOptions));
  /* eslint-disable-next-line @typescript-eslint/no-unused-vars */
  router.use((err: jayson.JSONRPCError, req, res, _next) => {
    const status = Number(err.code > 0 ? err.code : 500);
    res.status(status);
    res.json({
      jsonrpc: '2.0',
      id: req.body.id,
      error: {
        code: status,
        message: err.message,
        data: err.data,
      },
    });
  });
  return router;
}

export async function createServer(createOptions?: MintblueOptions): Promise<{ app: Application; server: Server }> {
  const router = await createRouter(createOptions);

  const app = express();

  app.use(router);

  const port = process.env.PORT || 3000;

  const server = app.listen(port, () => {
    console.log(`Mintblue SDK Server listening on port ${port}`);
  });

  return { app, server };
}
