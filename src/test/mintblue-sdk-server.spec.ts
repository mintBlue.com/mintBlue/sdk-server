import * as S from '../lib/server';

describe('Mintblue SDK Server', async () => {
  it('Can instantiate SDK Server', async () => {
    const server = await S.createServer();

    server.server.close();
  });
});
