import fs from 'fs';

import { MintblueOptions } from '@mintblue/sdk';

import { createServer } from '../index.js';

const args = process.argv.slice(2);

const [createOptionsFile] = args;

if (createOptionsFile) {
  const createOptions: MintblueOptions = JSON.parse(fs.readFileSync(createOptionsFile).toString('utf-8'));
  if (createOptions) {
    console.log(`Starting server in standalone mode with token ${createOptions.token.split('.')[0]}`);
    createServer(createOptions);
  } else {
    console.log('Error with token');
    process.exit(1);
  }
} else if (process.env.MINTBLUE_SDK_TOKEN) {
  console.log(`Starting server in standalone mode with token ${process.env.MINTBLUE_SDK_TOKEN.split('.')[0]}`);
  createServer({ token: process.env.MINTBLUE_SDK_TOKEN });
} else {
  console.log('Starting server in transactional mode. Provide token via "mintblue-sdk-key" HTTP header');
  createServer();
}
