FROM node:18 as builder
WORKDIR /opt/mintblue
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM node:18
WORKDIR /opt/mintblue
COPY --from=builder /opt/mintblue/dist/cjs/ ./
COPY --from=builder /opt/mintblue/package*.json ./
RUN npm ci --omit=dev
CMD ["node", "/opt/mintblue/bin/mintblue-sdk-server.js"]