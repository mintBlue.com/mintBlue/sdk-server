module.exports = {
  require: 'ts-node/register',
  extensions: ['js', 'ts'],
  'node-option': ['experimental-specifier-resolution=node', 'loader=ts-node/esm'],
};
