#!/bin/bash

set -ex
USERNAME=mintbluedevs
IMAGE=sdk-server

version=$(grep version package.json | cut -d\" -f4)

docker build -t $USERNAME/$IMAGE:latest .

docker tag $USERNAME/$IMAGE:latest $USERNAME/$IMAGE:$version

docker push $USERNAME/$IMAGE:latest
docker push $USERNAME/$IMAGE:$version