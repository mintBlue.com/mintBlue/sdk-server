# mintBlue SDK Server

[mintBlue](https://mintblue.com/) is a blockchain development platform that offers out-of-the-box infrastructure for businesses. The mintBlue SDK Server is a REST API that allows you to use the functionalities of the [mintBlue SDK](https://www.npmjs.com/package/@mintblue/sdk) without integrating the SDK package into your solution. This is particularly useful for decreasing implementation time, setting up proof of concepts, or easily integrating the functionality into non-JavaScript environments.

The mintBlue SDK Server implements the JSON-RPC specification, providing a uniform set of methods that clients can use regardless of the programming language they are implementing. JSON-RPC is a stateless, light-weight remote procedure call (RPC) protocol that defines several data structures and the rules around their processing.

## Features

- Create blockchain transactions
- Encrypt and digitally sign payloads locally
- Fetch transactions from the blockchain and decrypt payloads locally
- Organize your transactions into projects
- Create access tokens

## Installation

To install the mintBlue SDK Server, run the following command:

```sh
npm install @mintblue/sdk-server
```
## Running the SDK Server

There are two modes in which you can run the mintBlue SDK Server: transactional mode and standalone mode.

### Transactional Mode

To run the mintBlue SDK Server in transactional mode, follow these steps:

1. Run the server:

```sh
mintblue-sdk-server
```
2. In all your requests, include your SDK token in the header `mintblue-sdk-token`.

### Standalone Mode

There are two ways to run the mintBlue SDK Server in standalone mode:

#### With Environment Variable

To run the mintBlue SDK Server in standalone mode using an environment variable, run the following command:

```sh
MINTBLUE_SDK_TOKEN=<YOUR SDK TOKEN> mintblue-sdk-server
```
#### With Options File

To run the mintBlue SDK Server in standalone mode using an options file, follow these steps:

1. Create a file called `options.json` with the following contents:

```json
{
  "token": "YOUR SDK TOKEN"
}
```
2. Run the server with the following arguments:
```sh
mintblue-sdk-server options.json
```
## Client Example

Here is an example of a client request using JSON RPC to create a blockchain transaction with the mintBlue SDK Server:

```json
{
  "jsonrpc": "2.0",
  "method": "createTransaction",
  "params": {
    "project_id": "YOUR PROJECT ID",
    "outputs": [
      {
        "type": "data",
        "value": "Hello world",
        "sign": true,
        "encrypt": true
      }
    ]
  },
  "id": 1
}
```
For more information on using the mintBlue SDK Server, visit the [mintBlue documentation](https://docs.mintblue.com/).